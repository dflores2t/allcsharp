﻿using static System.Console;
using UtilityLibraries;
namespace HelloWorld{
    class program{
        static void Main(string[] arg){
            //     Console.WriteLine("Hello World!");

            //     WriteLine("Cuale se tu nombre");
            //     var name = Console.ReadLine();
            //     var currentDate = DateTime.Now;
            //     WriteLine(currentDate);
            //     WriteLine($"{Environment.NewLine}Hello, {name}, on {currentDate:d} at {currentDate:t}!");
            //     Write($"{Environment.NewLine} press any key to exit...");
            //     ReadKey(true);
            int row = 0;
            do
            {
                if (row == 0 || row >= 25)
                {
                    ResetConsole();
                }
                string? input = ReadLine();
                if (string.IsNullOrEmpty(input)) break;
                WriteLine($"Input: {input}");
                WriteLine("Begins with uppercase? " + $"{(input.StartsWithUpper() ? "Yes" : "No")}");
                WriteLine();
                row += 4;
            } while (true);
            return;

            //DECLARE A ResetConsole local method
            void ResetConsole(){
                if(row >0){
                    WriteLine("Press any ket to continue...");
                    ReadKey();
                }
                Clear();
                WriteLine($"{Environment.NewLine} press <Enter> only to exit; otherwise, enter a string and press <Enter>:{Environment.NewLine}");
            }
        }
}
             }